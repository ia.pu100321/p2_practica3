package com.sv.edu.ufg.fis.amb.p2_practica3_actionbar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.Toolbar

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbal)

        setSupportActionBar(toolbar)

        supportActionBar?.title = "Dennis Urquiza"
        supportActionBar?.subtitle = "Universidad Francisco Gavidia"
        supportActionBar?.setIcon(R.drawable.ic_launcher_background)

        val boton = findViewById<Button>(R.id.btn)

        boton.setOnClickListener{
            val intent = Intent(this, SegundaPantalla::class.java)

            startActivity(intent)
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.ufg_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.configuracion -> {
                Toast.makeText(this, "Usted ha seleccionado configuracion", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.configuracion_perfil -> {
                Toast.makeText(this, "Usted ha seleccionado configuracion perfil", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.mapa -> {
                Toast.makeText(this, "Usted ha seleccionado enviar ubicacion", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.nueva_cuenta -> {
                Toast.makeText(this, "Usted ha seleccionado agregar cuenta", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.salir -> {
                Toast.makeText(this, "Usted ha seleccionado salir", Toast.LENGTH_SHORT).show()
                true
            }
            else -> false
        }
    }
}